package main;

import gameentity.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class GameStage extends JPanel implements ActionListener, MouseListener {
	public static int[][] arr;
	public static int[][] importantKey;
	private Image imgLevel;
	private Player player;
	private JFrame frame;

	private Timer timer;
	private final int DELAY = 10;

	public List<Enemy> listEnemies = new LinkedList<>();
	public List<Tower> listTowers  = new LinkedList<>();

	private int status = 0;
	// 0: begin status
	// 1: gun1
	// 2: gun2
	// 3: gun3
	// 4: gun4
	// 5: sell
	// 6: upgrade
	// 7: sound
	// 9: map
	// 10: tower

	public boolean isRunning;
	public static boolean sound = true;
	public int numPart;
	public int numMaxPart;

	private Point coordinateMouse = new Point();
	private Tower tempTower;

	public GameStage(int level, Player player, JFrame frame, int numMaxPart) {
		addMouseListener(this);
		this.player = player;
		this.frame = frame;
		setupGameStage(level);
		setImgLevel(level);
		setSize(1200, 800);
		numPart = 1;
		this.numMaxPart = numMaxPart;
		sound = true;
		isRunning = false;

		JButton gun1 = new JButton();
		gun1.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/gun1.png"));
		gun1.setBounds(1002, 96, 64, 64);
		gun1.addActionListener(e -> {if (player.getCash() >= 10) status = 1; else status = -1;});
		frame.add(gun1);

		JButton gun2 = new JButton();
		gun2.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/gun2.png"));
		gun2.setBounds(1072, 96, 64, 64);
		frame.add(gun2);
		gun2.addActionListener(e -> {if (player.getCash() >= 20) status = 2; else status = -2;});

		JButton gun3 = new JButton();
		gun3.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/gun3.png"));
		gun3.setBounds(1002, 166, 64, 64);
		frame.add(gun3);
		gun3.addActionListener(e -> {if (player.getCash() >= 50) status = 3; else status = -3;});

		JButton gun4 = new JButton();
		gun4.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/gun4.png"));
		gun4.setBounds(1072, 166, 64, 64);
		frame.add(gun4);
		gun4.addActionListener(e -> {if (player.getCash() >= 75) status = 4; else status = -4;});

		JButton playAgain = new JButton();
		playAgain.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/PlayAgain.png"));
		playAgain.setBounds(992, 587, 118, 34);
		frame.add(playAgain);
		playAgain.addActionListener(e -> {
			status = 0;
			Main.status = 6;
		});

		JButton sound = new JButton();
		sound.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/Sound.png"));
		sound.setBounds(1120, 576, 50, 49);
		frame.add(sound);
		sound.addActionListener(e -> {
			GameStage.sound = !GameStage.sound;
			if (!GameStage.sound) {
				Main.music.stop();
			} else Main.music.loop(100000);
		});

		JButton sell = new JButton();
		sell.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/Sell.png"));
		sell.setBounds(999, 528, 56, 38);
		frame.add(sell);
		sell.addActionListener(e -> {
			player.setCash(player.getCash() + tempTower.getSell());
			arr[(tempTower.getX() - 64)/64][(tempTower.getY() - 64)/64] = 0;
			listTowers.remove(tempTower);
			status = 0;
		});

		JButton upgrade = new JButton();
		upgrade.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/Upgrade.png"));
		upgrade.setBounds(1083, 528, 76, 38);
		frame.add(upgrade);
		upgrade.addActionListener(e -> {
			if (tempTower.getKeyLevel() < tempTower.getMaxLevel() && player.getCash() >= tempTower.getUpgrade()) {
				player.setCash(player.getCash() - tempTower.getUpgrade());
				tempTower.Upgrade();
			} else {
				status = 0;
				return;
			}
		});

		JButton go = new JButton();
		go.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/Go.png"));
		go.setBounds(9*64, 512+64+32, 42, 29);
		frame.add(go);
		go.addActionListener(e -> {
			if (!isRunning) {
				setupEnemy(player.getLevel());
				isRunning = true;
			}
		});
		
		JButton buyLife = new JButton();
		buyLife.setIcon(new ImageIcon("Asset/buttons/FitSizeButton/BuyLife.png"));
		buyLife.setBounds(999+16, 528-48, 118, 34);
		frame.add(buyLife);
		buyLife.addActionListener(e -> {
		    if (player.getCash() >= 50) {
		        player.setCash(player.getCash() - 50);
				player.setEnergy(player.getEnergy() + 1);
		    }
		}
		);

		timer = new Timer(DELAY, this);
		timer.start();
	}

	public void setImgLevel(int level) {
		switch (level) {
			case 1:
				imgLevel = new ImageIcon("Asset/Level/level1.png").getImage();
				break;
			case 2:
				imgLevel = new ImageIcon("Asset/Level/level2.png").getImage();
				break;
			case 3:
				imgLevel = new ImageIcon("Asset/Level/level3.png").getImage();
				break;
		}
	}

	public void setupEnemy(int level) {
		switch (level) {
			case 1:
				switch (numPart) {
					case 1:
						listEnemies.add(new Car(13*64+64, 1*64+64+10, 5, 100, 3, 4));
						listEnemies.add(new Car(14*64+64, 1*64+64+10, 5, 100, 3, 4));
						listEnemies.add(new Car(15*64+64, 1*64+64+10, 5, 100, 3, 4));
						listEnemies.add(new Car(16*64+64, 1*64+64+10, 5, 100, 3, 4));
						listEnemies.add(new Car(18*64+64, 1*64+64+10, 5, 200, 5, 4));
						listEnemies.add(new Car(26*64+64, 1*64+64+10, 6, 200, 5, 4));
						listEnemies.add(new Car(27*64+64, 1*64+64+10, 6, 200, 5, 4));
						listEnemies.add(new Car(45*64+64, 1*64+64+10, 8, 300, 6, 4));
						listEnemies.add(new Car(47*64+64, 1*64+64+10, 8, 300, 6, 4));
						listEnemies.add(new Tank(40*64+64, 1*64+64+10, 5, 500, 20, 4));
						break;
					case 2:
						listEnemies.add(new Car(13*64+64, 1*64+64+10, 6, 150, 5, 4));
						listEnemies.add(new Car(14*64+64, 1*64+64+10, 6, 150, 5, 4));
						listEnemies.add(new Car(15*64+64, 1*64+64+10, 6, 150, 5, 4));
						listEnemies.add(new Car(20*64+64, 1*64+64+10, 7, 300, 5, 4));
						listEnemies.add(new Car(22*64+64, 1*64+64+10, 7, 300, 8, 4));
						listEnemies.add(new Car(23*64+64, 1*64+64+10, 7, 300, 8, 4));
						listEnemies.add(new Car(25*64+64, 1*64+64+10, 7, 300, 8, 4));
						listEnemies.add(new Tank(25*64+64, 1*64+64+10, 6, 700, 20, 4));
						listEnemies.add(new Tank(27*64+64, 1*64+64+10, 5, 700, 20, 4));
						listEnemies.add(new Tank(30*64+64, 1*64+64+10, 4, 1000, 30, 4));
						break;
					case 3:
						listEnemies.add(new Car(13*64+64, 1*64+64+10, 8, 700, 8, 4));
						listEnemies.add(new Car(14*64+64, 1*64+64+10, 8, 700, 8, 4));
						listEnemies.add(new Car(15*64+64, 1*64+64+10, 8, 700, 8, 4));
						listEnemies.add(new Plane(20*64+64, 1*64+64+10, 10, 200, 10, 4));
						listEnemies.add(new Plane(22*64+64, 1*64+64+10, 10, 200, 10, 4));
						listEnemies.add(new Tank(20*64+64, 1*64+64+10, 7, 2000, 40, 4));
						listEnemies.add(new Tank(21*64+64, 1*64+64+10, 7, 2000, 40, 4));
						break;
					case 4:
						listEnemies.add(new Car(13*64+64, 1*64+64+10, 10, 800, 8, 4));
						listEnemies.add(new Car(14*64+64, 1*64+64+10, 10, 800, 8, 4));
						listEnemies.add(new Car(15*64+64, 1*64+64+10, 10, 800, 8, 4));
						listEnemies.add(new Car(18*64+64, 1*64+64+10, 10, 800, 8, 4));
						listEnemies.add(new Car(19*64+64, 1*64+64+10, 10, 800, 8, 4));
						listEnemies.add(new Tank(18*64+64, 1*64+64+10, 7, 1500, 20, 4));
						listEnemies.add(new Tank(19*64+64, 1*64+64+10, 7, 1500, 20, 4));
						listEnemies.add(new Tank(21*64+64, 1*64+64+10, 7, 1800, 25, 4));
						listEnemies.add(new Tank(21*64+64, 1*64+64+10, 5, 2200, 50, 4));
						listEnemies.add(new Tank(22*64+64, 1*64+64+10, 5, 2200, 50, 4));
						break;
					case 5:
						listEnemies.add(new Plane(13*64+64, 1*64+64+10, 14, 400, 15, 4));
						listEnemies.add(new Plane(14*64+64, 1*64+64+10, 14, 400, 15, 4));
						listEnemies.add(new Plane(15*64+64, 1*64+64+10, 14, 400, 15, 4));
						listEnemies.add(new Plane(16*64+64, 1*64+64+10, 14, 400, 15, 4));
						listEnemies.add(new Plane(17*64+64, 1*64+64+10, 14, 400, 15, 4));
						listEnemies.add(new Plane(18*64+64, 1*64+64+10, 14, 400, 15, 4));
						listEnemies.add(new Plane(19*64+64, 1*64+64+10, 14, 400, 15, 4));
						listEnemies.add(new Plane(20*64+64, 1*64+64+10, 14, 400, 15, 4));
						break;
				}
				break;

			case 2:
				switch (numPart) {
					case 1:
						listEnemies.add(new Car(13*64+64, 1*64+64+10, 6, 200, 5, 4));
						listEnemies.add(new Car(14*64+64, 1*64+64+10, 6, 200, 5, 4));
						listEnemies.add(new Car(15*64+64, 1*64+64+10, 6, 200, 5, 4));
						listEnemies.add(new Car(16*64+64, 1*64+64+10, 6, 200, 5, 4));
						listEnemies.add(new Car(19*64+64, 1*64+64+10, 6, 250, 7, 4));
						listEnemies.add(new Car(20*64+64, 1*64+64+10, 6, 250, 7, 4));
						listEnemies.add(new Car(21*64+64, 1*64+64+10, 6, 300, 8, 4));
						listEnemies.add(new Car(26*64+64, 1*64+64+10, 6, 300, 8, 4));
						listEnemies.add(new Car(27*64+64, 1*64+64+10, 6, 300, 8, 4));
						listEnemies.add(new Tank(26*64+64, 1*64+64+10, 5, 1000, 20, 4));
						break;
					case 2:
						listEnemies.add(new Car(14*64+64, 1*64+64+10, 7, 300, 8, 4));
						listEnemies.add(new Car(15*64+64, 1*64+64+10, 7, 300, 8, 4));
						listEnemies.add(new Car(16*64+64, 1*64+64+10, 7, 300, 8, 4));
						listEnemies.add(new Car(20*64+64, 1*64+64+10, 7, 350, 10, 4));
						listEnemies.add(new Car(21*64+64, 1*64+64+10, 7, 350, 10, 4));
						listEnemies.add(new Tank(20*64+64, 1*64+64+10, 6, 1200, 22, 4));
						listEnemies.add(new Tank(21*64+64, 1*64+64+10, 6, 1200, 22, 4));
						break;
					case 3:
						listEnemies.add(new Car(14*64+64, 1*64+64+10, 9, 500, 7, 4));
						listEnemies.add(new Car(15*64+64, 1*64+64+10, 9, 500, 7, 4));
						listEnemies.add(new Car(16*64+64, 1*64+64+10, 9, 500, 7, 4));
						listEnemies.add(new Car(19*64+64, 1*64+64+10, 10, 800, 8, 4));
						listEnemies.add(new Car(20*64+64, 1*64+64+10, 10, 800, 8, 4));
						listEnemies.add(new Car(21*64+64, 1*64+64+10, 10, 800, 8, 4));
						listEnemies.add(new Car(25*64+64, 1*64+64+10, 10, 800, 10, 4));
						listEnemies.add(new Car(26*64+64, 1*64+64+10, 10, 800, 10, 4));
						listEnemies.add(new Plane(30*64+64, 1*64+64+10, 11, 2500, 20, 4));
						listEnemies.add(new Plane(32*64+64, 1*64+64+10, 11, 2500, 20, 4));
						break;
					case 4:
						listEnemies.add(new Tank(13*64+64, 1*64+64+10, 10, 550, 10, 4));
						listEnemies.add(new Tank(14*64+64, 1*64+64+10, 10, 550, 10, 4));
						listEnemies.add(new Tank(15*64+64, 1*64+64+10, 10, 550, 10, 4));
						listEnemies.add(new Tank(16*64+64, 1*64+64+10, 10, 600, 11, 4));
						listEnemies.add(new Tank(17*64+64, 1*64+64+10, 10, 600, 11, 4));
						listEnemies.add(new Tank(19*64+64, 1*64+64+10, 9, 1700, 15, 4));
						listEnemies.add(new Tank(21*64+64, 1*64+64+10, 9, 1700, 15, 4));
						break;
					case 5:
						listEnemies.add(new Car(13*64+64, 1*64+64+10, 12, 700, 8, 4));
						listEnemies.add(new Car(14*64+64, 1*64+64+10, 12, 700, 8, 4));
						listEnemies.add(new Car(15*64+64, 1*64+64+10, 12, 700, 8, 4));
						listEnemies.add(new Car(16*64+64, 1*64+64+10, 12, 700, 8, 4));
						listEnemies.add(new Plane(20*64+64, 1*64+64+10, 13, 750, 10, 4));
						listEnemies.add(new Plane(21*64+64, 1*64+64+10, 13, 750, 10, 4));
						listEnemies.add(new Plane(22*64+64, 1*64+64+10, 13, 750, 10, 4));
						listEnemies.add(new Plane(23*64+64, 1*64+64+10, 13, 750, 10, 4));
						listEnemies.add(new Plane(24*64+64, 1*64+64+10, 13, 1900, 15, 4));
						listEnemies.add(new Plane(25*64+64, 1*64+64+10, 13, 1900, 15, 4));
						listEnemies.add(new Tank(25*64+64, 1*64+64+10, 10, 10000, 20, 4));
						listEnemies.add(new Tank(27*64+64, 1*64+64+10, 10, 10000, 20, 4));
						break;
				}
				break;

			case 3:
				switch (numPart) {
					case 1:
						listEnemies.add(new Car(13*64+64, 0*64+64+10, 6, 200, 2, 4));
						listEnemies.add(new Car(14*64+64, 0*64+64+10, 6, 200, 2, 4));
						listEnemies.add(new Car(15*64+64, 0*64+64+10, 6, 200, 2, 4));
						listEnemies.add(new Car(16*64+64, 0*64+64+10, 6, 200, 2, 4));
						listEnemies.add(new Car(20*64+64, 0*64+64+10, 7, 250, 3, 4));
						listEnemies.add(new Car(21*64+64, 0*64+64+10, 7, 250, 3, 4));
						listEnemies.add(new Car(22*64+64, 0*64+64+10, 7, 250, 3, 4));
						listEnemies.add(new Car(28*64+64, 0*64+64+10, 7, 300, 5, 4));
						listEnemies.add(new Car(29*64+64, 0*64+64+10, 7, 300, 5, 4));
						listEnemies.add(new Tank(29*64+64, 0*64+64+10, 5, 1000, 13, 4));
						break;
					case 2:
						listEnemies.add(new Car(13*64+64, 0*64+64+10, 7, 400, 4, 4));
						listEnemies.add(new Car(14*64+64, 0*64+64+10, 7, 400, 4, 4));
						listEnemies.add(new Car(15*64+64, 0*64+64+10, 7, 400, 4, 4));
						listEnemies.add(new Car(16*64+64, 0*64+64+10, 7, 400, 4, 4));
						listEnemies.add(new Car(25*64+64, 0*64+64+10, 8, 450, 5, 4));
						listEnemies.add(new Car(26*64+64, 0*64+64+10, 8, 450, 5, 4));
						listEnemies.add(new Car(27*64+64, 0*64+64+10, 8, 450, 5, 4));
						listEnemies.add(new Tank(28*64+64, 0*64+64+10, 5, 1500, 15, 4));
						break;
					case 3:
						listEnemies.add(new Tank(13*64+64, 0*64+64+10, 8, 1000, 7, 4));
						listEnemies.add(new Tank(14*64+64, 0*64+64+10, 8, 1000, 7, 4));
						listEnemies.add(new Tank(16*64+64, 0*64+64+10, 8, 1000, 7, 4));
						listEnemies.add(new Tank(18*64+64, 0*64+64+10, 8, 1200, 8, 4));
						listEnemies.add(new Tank(19*64+64, 0*64+64+10, 8, 1200, 8, 4));
						listEnemies.add(new Tank(20*64+64, 0*64+64+10, 8, 1200, 8, 4));
						listEnemies.add(new Tank(21*64+64, 0*64+64+10, 7, 2000, 14, 4));
						listEnemies.add(new Tank(22*64+64, 0*64+64+10, 7, 2000, 14, 4));
						listEnemies.add(new Tank(23*64+64, 0*64+64+10, 7, 2000, 14, 4));
						listEnemies.add(new Tank(24*64+64, 0*64+64+10, 7, 2000, 14, 4));
						break;
					case 4:
						listEnemies.add(new Car(14*64+64, 0*64+64+10, 9, 850, 11, 4));
						listEnemies.add(new Car(15*64+64, 0*64+64+10, 9, 850, 11, 4));
						listEnemies.add(new Car(16*64+64, 0*64+64+10, 9, 850, 11, 4));
						listEnemies.add(new Car(17*64+64, 0*64+64+10, 9, 850, 11, 4));
						listEnemies.add(new Tank(15*64+64, 0*64+64+10, 7, 1800, 25, 4));
						listEnemies.add(new Car(30*64+64, 0*64+64+10, 10, 900, 12, 4));
						listEnemies.add(new Car(31*64+64, 0*64+64+10, 10, 900, 12, 4));
						listEnemies.add(new Car(32*64+64, 0*64+64+10, 10, 900, 12, 4));
						listEnemies.add(new Tank(25*64+64, 0*64+64+10, 8, 1900, 26, 4));
						listEnemies.add(new Car(40*64+64, 0*64+64+10, 10, 950, 14, 4));
						listEnemies.add(new Car(42*64+64, 0*64+64+10, 10, 950, 14, 4));
						listEnemies.add(new Tank(43*64+64, 0*64+64+10, 8, 2000, 28, 4));
						listEnemies.add(new Plane(35*64+64, 0*64+64+10, 11, 550, 15, 4));
						break;
					case 5:
						listEnemies.add(new Car(14*64+64, 0*64+64+10, 10, 1050, 16, 4));
						listEnemies.add(new Car(15*64+64, 0*64+64+10, 10, 1050, 16, 4));
						listEnemies.add(new Car(16*64+64, 0*64+64+10, 10, 1050, 16, 4));
						listEnemies.add(new Car(17*64+64, 0*64+64+10, 10, 1050, 16, 4));
						listEnemies.add(new Tank(13*64+64, 0*64+64+10, 8, 2200, 30, 4));
						listEnemies.add(new Car(19*64+64, 0*64+64+10, 11, 1100, 17, 4));
						listEnemies.add(new Car(20*64+64, 0*64+64+10, 11, 1100, 17, 4));
						listEnemies.add(new Car(21*64+64, 0*64+64+10, 11, 1100, 17, 4));
						listEnemies.add(new Tank(18*64+64, 0*64+64+10, 9, 2300, 31, 4));
						listEnemies.add(new Plane(25*64+64, 0*64+64+10, 12, 700, 20, 4));
						listEnemies.add(new Car(23*64+64, 0*64+64+10, 11, 1150, 19, 4));
						listEnemies.add(new Car(24*64+64, 0*64+64+10, 11, 1150, 19, 4));
						listEnemies.add(new Tank(22*64+64, 0*64+64+10, 9, 2400, 33, 4));
						listEnemies.add(new Plane(26*64+64, 0*64+64+10, 12, 800, 22, 4));
						break;
					case 6:
						listEnemies.add(new Car(14*64+64, 0*64+64+10, 11, 1250, 22, 4));
						listEnemies.add(new Car(15*64+64, 0*64+64+10, 11, 1250, 22, 4));
						listEnemies.add(new Car(16*64+64, 0*64+64+10, 11, 1250, 22, 4));
						listEnemies.add(new Car(17*64+64, 0*64+64+10, 11, 1250, 22, 4));
						listEnemies.add(new Tank(13*64+64, 0*64+64+10, 9, 2600, 36, 4));
						listEnemies.add(new Car(19*64+64, 0*64+64+10, 12, 1300, 23, 4));
						listEnemies.add(new Car(20*64+64, 0*64+64+10, 12, 1300, 23, 4));
						listEnemies.add(new Car(21*64+64, 0*64+64+10, 12, 1300, 23, 4));
						listEnemies.add(new Tank(18*64+64, 0*64+64+10, 10, 2700, 37, 4));
						listEnemies.add(new Plane(26*64+64, 0*64+64+10, 13, 950, 26, 4));
						listEnemies.add(new Car(23*64+64, 0*64+64+10, 12, 1350, 25, 4));
						listEnemies.add(new Car(24*64+64, 0*64+64+10, 12, 1350, 25, 4));
						listEnemies.add(new Tank(22*64+64, 0*64+64+10, 10, 2800, 38, 4));
						listEnemies.add(new Tank(25*64+64, 0*64+64+10, 10, 2800, 38, 4));
						listEnemies.add(new Plane(27*64+64, 0*64+64+10, 14, 1100, 27, 4));
						break;
					case 7:
						listEnemies.add(new Car(14*64+64, 0*64+64+10, 12, 1450, 29, 4));
						listEnemies.add(new Car(15*64+64, 0*64+64+10, 12, 1450, 29, 4));
						listEnemies.add(new Car(16*64+64, 0*64+64+10, 12, 1450, 29, 4));
						listEnemies.add(new Car(17*64+64, 0*64+64+10, 12, 1450, 29, 4));
						listEnemies.add(new Car(18*64+64, 0*64+64+10, 12, 1450, 29, 4));
						listEnemies.add(new Tank(13*64+64, 0*64+64+10, 10, 3000, 45, 4));
						listEnemies.add(new Car(20*64+64, 0*64+64+10, 13, 1500, 30, 4));
						listEnemies.add(new Car(21*64+64, 0*64+64+10, 13, 1500, 30, 4));
						listEnemies.add(new Car(24*64+64, 0*64+64+10, 13, 1500, 30, 4));
						listEnemies.add(new Tank(19*64+64, 0*64+64+10, 11, 3100, 46, 4));
						listEnemies.add(new Plane(25*64+64, 0*64+64+10, 14, 1250, 33, 4));
						listEnemies.add(new Car(27*64+64, 0*64+64+10, 13, 1550, 32, 4));
						listEnemies.add(new Car(28*64+64, 0*64+64+10, 13, 1550, 32, 4));
						listEnemies.add(new Tank(23*64+64, 0*64+64+10, 11, 3200, 48, 4));
						listEnemies.add(new Tank(26*64+64, 0*64+64+10, 11, 3200, 48, 4));
						listEnemies.add(new Plane(29*64+64, 0*64+64+10, 15, 1350, 34, 4));
						break;
					case 8:
						listEnemies.add(new Car(13*64+64, 0*64+64+10, 13, 1650, 37, 4));
						listEnemies.add(new Car(14*64+64, 0*64+64+10, 13, 1650, 37, 4));
						listEnemies.add(new Car(15*64+64, 0*64+64+10, 13, 1650, 37, 4));
						listEnemies.add(new Car(16*64+64, 0*64+64+10, 13, 1650, 37, 4));
						listEnemies.add(new Car(17*64+64, 0*64+64+10, 13, 1650, 37, 4));
						listEnemies.add(new Car(18*64+64, 0*64+64+10, 13, 1650, 37, 4));
						listEnemies.add(new Car(23*64+64, 0*64+64+10, 14, 1700, 38, 4));
						listEnemies.add(new Car(24*64+64, 0*64+64+10, 14, 1700, 38, 4));
						listEnemies.add(new Tank(19*64+64, 0*64+64+10, 11, 3400, 53, 4));
						listEnemies.add(new Tank(20*64+64, 0*64+64+10, 11, 3400, 53, 4));
						listEnemies.add(new Tank(21*64+64, 0*64+64+10, 11, 3400, 53, 4));
						listEnemies.add(new Tank(22*64+64, 0*64+64+10, 11, 3400, 53, 4));
						listEnemies.add(new Car(25*64+64, 0*64+64+10, 15, 1750, 40, 4));
						listEnemies.add(new Car(26*64+64, 0*64+64+10, 15, 1750, 40, 4));
						listEnemies.add(new Car(27*64+64, 0*64+64+10, 15, 1750, 40, 4));
						listEnemies.add(new Plane(28*64+64, 0*64+64+10, 15, 1500, 41, 4));
						listEnemies.add(new Plane(29*64+64, 0*64+64+10, 15, 1500, 41, 4));
						break;
				}
				break;
		}
	}

	public void setupGameStage(int level) {
		// 1: Road
		// 2: Spawn
		// 3: Target

		switch (level) {
			case 1:
		 /* 0 0 0 0 0 0 0 0 0 0 0 0 0 0
			0 0 0 0 0 0 0 0 0 1 1 1 1 2
			0 0 0 0 0 0 0 0 0 1 0 0 0 0
			0 0 0 1 1 1 1 1 0 1 0 0 0 0
			0 0 0 1 0 0 0 1 1 1 0 0 0 0
			0 0 0 1 0 0 0 0 0 0 0 0 0 0
			3 1 1 1 0 0 0 0 0 0 0 0 0 0
			0 0 0 0 0 0 0 0 0 0 0 0 0 0 */


				arr = new int[][] {
						{0, 0, 0, 0, 0, 0, 3, 0},
						{0, 0, 0, 0, 0, 0, 1, 0},
						{0, 0, 0, 0, 0, 0, 1, 0},
						{0, 0, 0, 1, 1, 1, 1, 0},
						{0, 0, 0, 1, 0, 0, 0, 0},
						{0, 0, 0, 1, 0, 0, 0, 0},
						{0, 0, 0, 1, 0, 0, 0, 0},
						{0, 0, 0, 1, 1, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0},
						{0, 1, 1, 1, 1, 0, 0, 0},
						{0, 1, 0, 0, 0, 0, 0, 0},
						{0, 1, 0, 0, 0, 0, 0, 0},
						{0, 1, 0, 0, 0, 0, 0, 0},
						{0, 2, 0, 0, 0, 0, 0, 0}
				};
				importantKey = new int[][] {
						{13, 1}, {9, 1}, {9, 4}, {7, 4}, {7, 3}, {3, 3}, {3, 6}, {0, 6}
				};

				break;

			case 2:
		 /* 0 0 0 0 0 0 0 0 0 0 0 0 0 0
			0 1 1 1 1 1 1 1 1 1 1 1 1 2
			0 1 0 0 0 0 0 0 0 0 0 0 0 0
			0 1 0 0 0 0 0 0 0 0 0 0 0 0
			0 1 1 1 1 1 1 1 1 1 1 1 1 0
			0 0 0 0 0 0 0 0 0 0 0 0 1 0
			0 0 0 0 0 0 0 0 0 0 0 0 1 0
			3 1 1 1 1 1 1 1 1 1 1 1 1 0 */


				arr = new int[][] {
						{0, 0, 0, 0, 0, 0, 0, 3},
						{0, 1, 1, 1, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 0, 0, 1},
						{0, 1, 0, 0, 1, 1, 1, 1},
						{0, 2, 0, 0, 0, 0, 0, 0}
				};
				importantKey = new int[][] {
						{13, 1}, {1, 1}, {1, 4}, {12, 4}, {12, 7}, {0, 7}
				};

				break;

			case 3:
		 /* 0 0 0 0 0 0 0 1 1 1 1 1 1 2
			0 0 0 0 0 0 0 1 0 0 0 0 0 0
			0 1 0 0 0 0 0 1 0 0 0 0 0 0
			0 1 0 0 0 0 0 1 0 0 0 0 0 0
			3 1 1 1 1 1 1 1 0 0 0 0 0 0
			0 0 0 0 0 0 0 0 0 0 0 0 0 0
			0 0 0 0 0 0 0 0 0 0 0 0 0 0
			0 0 0 0 0 0 0 0 0 0 0 0 0 0 */


				arr = new int[][] {
						{0, 0, 0, 0, 3, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0},
						{0, 0, 0, 0, 1, 0, 0, 0},
						{1, 1, 1, 1, 1, 0, 0, 0},
						{1, 0, 0, 0, 0, 0, 0, 0},
						{1, 0, 0, 0, 0, 0, 0, 0},
						{1, 0, 0, 0, 0, 0, 0, 0},
						{1, 0, 0, 0, 0, 0, 0, 0},
						{1, 0, 0, 0, 0, 0, 0, 0},
						{2, 0, 0, 0, 0, 0, 0, 0}
				};
				importantKey = new int[][]{
						{13, 0}, {7, 0}, {7, 4}, {0, 4}
				};
				break;
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		g.drawImage(new ImageIcon("newUntitled.png").getImage(), 0, 0, 1200, 800, null);
		g.drawImage(imgLevel, 64, 64, 896, 512, null);
		g.drawImage(new ImageIcon("Asset/shapes/Board.png").getImage(), 970, 64, null);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/gun1.png").getImage(), 1002, 96, null);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/gun2.png").getImage(), 1072, 96, null);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/gun3.png").getImage(), 1002, 166, null);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/gun4.png").getImage(), 1072, 166, null);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/PlayAgain.png").getImage(), 992, 587, null);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/Sound.png").getImage(), 1120, 576, null);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/Sell.png").getImage(), 999, 528, null);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/Upgrade.png").getImage(), 1083, 528, null);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/BuyLife.png").getImage(), 999+16, 528-48, null);

		String info = "Energy: " + player.getEnergy() + "      Cash: $" + player.getCash() + "      Level: " + player.getLevel() + "      Part: " + numPart + "/" + numMaxPart;
		g.drawString(info, 64, 640);
		g.drawImage(new ImageIcon("Asset/buttons/FitSizeButton/Go.png").getImage(), 576, 608, null);

		if (!sound) {
			g.drawLine(1125,565,1170,630);
		}

		if (status == 10) {
			tempTower.printInfor(g);
			Color oldColor = g.getColor();
			g.setColor(new Color(190, 233, 192));
			g.drawOval(tempTower.getX() + 32 - tempTower.getRadius(), tempTower.getY() + 32 - tempTower.getRadius(), 2*tempTower.getRadius(), 2*tempTower.getRadius());
			g.setColor(oldColor);
		}

		if (status != 0 && status*status <= 16) {
			Tower.printInfor(g, Math.abs(status));
		}

		if (status >= 1 && status <= 4) {
			coordinateMouse.x = (MouseInfo.getPointerInfo().getLocation().x - frame.getX() - 32);
			coordinateMouse.y = (MouseInfo.getPointerInfo().getLocation().y - frame.getY() - 50);
			g.drawImage(new ImageIcon("Asset/GameEntity/platformGun.png").getImage(), coordinateMouse.x, coordinateMouse.y, null);
		}

		for (int i = 0; i < listEnemies.size(); i++) {
			Enemy enemy = listEnemies.get(i);
			if (enemy.getBlood() > 0) {
				if (inRange(enemy.getX(), enemy.getY()))
					enemy.paintComponent(g);
			}
			else {
				player.setCash(player.getCash() + enemy.getReward());
				listEnemies.remove(enemy);
			}
		}
		for (int i = 0; i < listTowers.size(); i++) {
			Tower tower = listTowers.get(i);
			boolean check = false;
			for (Enemy enemy : listEnemies) {
				if (tower.canShoot(enemy)) {
					tower.paintComponent(g, Math.toDegrees(tower.getAngleinRadian(enemy)), enemy);
					enemy.reduceBlood(tower.getDamage());
					check = true;
					break;
				}
			}
			if (check == false) {
				tower.paintComponent(g, 0, null);
			}
		}

		for (int i = 0; i < listEnemies.size(); i++) {
			Enemy enemy = listEnemies.get(i);
			if (player.getEnergy() > 0) {
				enemy.move(importantKey, player, listEnemies);
			}
			else {
				status = 0;
				Main.status = 4;
				break;
			}
		}
		if (listEnemies.size() == 0 && isRunning) {
			if (numPart < numMaxPart) {
				numPart++;
				setupEnemy(player.getLevel());
			} else {
				try {
					// 3 la so man choi nhieu nhat trong thoi diem hien tai
					if (player.getLevel() == 3) {
						frame.setVisible(false);
						FileWriter file = new FileWriter("Game.txt");
						file.write('1');
						file.close();
						status = 0;
						Main.status = 5;
					}
					else {
						FileWriter file = new FileWriter("Game.txt");
						file.write((char) (player.getLevel() + 1 + '0'));
						file.close();
						status = 0;
						Main.status = 3;
					}

				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			Thread.sleep(70);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		repaint();

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (status == 1 || status == 2 || status == 3 || status == 4) {
			int i = (e.getX() - 64)/64;
			int j = (e.getY() - 64)/64;
			if (i >= 0 && i < 14 && j >= 0 && j < 8 && arr[i][j] == 0) {
				switch (status) {
					case 1:
						listTowers.add(new Gun1(i*64 + 64, j*64 + 64));
						player.setCash(player.getCash() - 10);
						arr[i][j] = 4;
						break;
					case 2:
						if (player.getCash() >= 20) {
							listTowers.add(new Gun2(i*64 + 64, j*64 + 64));
							player.setCash(player.getCash() - 20);
							arr[i][j] = 4;
						}
						break;
					case 3:
						if (player.getCash() >= 50) {
							listTowers.add(new Gun3(i*64 + 64, j*64 + 64));
							player.setCash(player.getCash() - 50);
							arr[i][j] = 4;
						}
						break;
					case 4:
						if (player.getCash() >= 75) {
							listTowers.add(new Gun4(i*64 + 64, j*64 + 64));
							player.setCash(player.getCash() - 75);
							arr[i][j] = 4;
						}
						break;
				}
			}
			status = 0;
		}
		status = 0;
		for (Tower tower : listTowers) {
			if (e.getX() >= tower.getX() && e.getX() <= tower.getX() + 64
					&& e.getY() >= tower.getY() && e.getY() <= tower.getY() + 64) {
				tempTower = tower;
				status = 10;
			}
		}

	}

	@Override
	public void mousePressed(MouseEvent e) {}
	@Override
	public void mouseReleased(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}

	private boolean inRange(int x, int y) {
		return (x <= 896);
	}
}
