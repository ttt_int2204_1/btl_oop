package main;

import javax.swing.*;
import java.io.FileReader;
import java.util.LinkedList;

public class NewGame {
    public static GameStage gameStage;
    public static Player player = new Player();
    public static int numMaxPart;
    public static void setupPlayer(int level) {
        switch (level) {
            case 1:
                player.setPlayer(5, 30, 1);
                numMaxPart = 5;
                break;
            case 2:
                player.setPlayer(5, 40, 2);
                numMaxPart = 5;
                break;
            case 3:
                player.setPlayer(8, 40, 3);
                numMaxPart = 8;
                break;
        }
        player.setLevel(level);
    }

    public static void setNewGame(JFrame frame) throws Exception{
        FileReader file = new FileReader("Game.txt");
        int level = (int) file.read() - '0';
        file.close();
        setupPlayer(level);
        if (gameStage != null) {
            gameStage.isRunning = false;
            gameStage.listEnemies = new LinkedList<>();
            gameStage.listTowers = new LinkedList<>();
            gameStage.numPart = 1;
            gameStage.setupGameStage(level);
            gameStage.setImgLevel(level);
        }
        else {
            gameStage = new GameStage(level, NewGame.player, frame, NewGame.numMaxPart);
            frame.add(gameStage);
        }
        frame.setVisible(true);
    }
}
