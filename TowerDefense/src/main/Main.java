package main;

import javax.sound.sampled.*;
import javax.swing.*;
import java.io.File;

public class Main {
    public static JFrame frame1 = new JFrame();
    public static JFrame frame2 = new JFrame();
    public static Clip music;
    public static int status = 1;
    // 0: continue
    // 1: Load home page
    // 2: Load game stage (first)
    // 3: Load next level
    // 4: Load end game
    // 5: Load win game
    // 6: play again

    public static void setFrame() {
        frame1 = new JFrame();
        frame1.setTitle("Tower Defense");
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame1.setSize(1200, 800);
        frame1.setIconImage(new ImageIcon("Asset/Icon/icon.png").getImage());
        frame1.setLocationRelativeTo(null);
        frame1.setVisible(true);
        frame2 = new JFrame();
        frame2.setTitle("Tower Defense");
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame2.setSize(1200, 800);
        frame2.setIconImage(new ImageIcon("Asset/Icon/icon.png").getImage());
        frame2.setLocationRelativeTo(null);
    }

    public static void setMusic() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("Asset/sounds/music.wav").getAbsoluteFile( ));
            music = AudioSystem.getClip( );
            music.open(audioInputStream);
            music.start();
            music.loop(100000);
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) throws Exception {
        setFrame();
        setMusic();

        while (true) {
            switch (Main.status) {
                case 0:
                    continue;
                case 1:
                    HomePage.setHomePage(frame1);
                    Main.status = 0;
                    break;
                case 2:
                    frame1.removeAll();
                    frame1.setVisible(false);
                    NewGame.setNewGame(frame2);
                    Main.status = 0;
                    break;
                case 3:
                    frame2.setVisible(false);
                    frame1.removeAll();
                    frame1 = new JFrame();
                    frame1.setTitle("Tower Defense");
                    frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame1.setSize(1200, 800);
                    frame1.setIconImage(new ImageIcon("Asset/Icon/icon.png").getImage());
                    frame1.setLocationRelativeTo(null);
                    NextLevel.setNextLevel(frame1);
                    frame1.setVisible(true);
                    Main.status = 0;
                    break;
                case 4:
                    frame2.setVisible(false);
                    frame1.removeAll();
                    frame1 = new JFrame();
                    frame1.setTitle("Tower Defense");
                    frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame1.setSize(1200, 800);
                    frame1.setIconImage(new ImageIcon("Asset/Icon/icon.png").getImage());
                    frame1.setLocationRelativeTo(null);
                    EndGame.setEndGame(frame1);
                    frame1.setVisible(true);
                    Main.status = 0;
                    break;
                case 5:
                    frame2.setVisible(false);
                    frame1.removeAll();
                    frame1 = new JFrame();
                    frame1.setTitle("Tower Defense");
                    frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame1.setSize(1200, 800);
                    frame1.setIconImage(new ImageIcon("Asset/Icon/icon.png").getImage());
                    frame1.setLocationRelativeTo(null);
                    WinGame.setWinGame(frame1);
                    frame1.setVisible(true);
                    Main.status = 0;
                    break;
                case 6:
                    frame1.setVisible(false);
                    frame2.setVisible(true);
                    NewGame.setNewGame(frame2);
                    Main.status = 0;
                    break;

            }
        }
    }
}
