package main;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class NextLevel {
    public static NextLevelPanel nextLevelPanel;
    public static void setNextLevel(JFrame frame) {
        nextLevelPanel = new NextLevelPanel();
        frame.add(nextLevelPanel);
        frame.setVisible(true);
    }
}

class NextLevelPanel extends JPanel {
    public NextLevelPanel() {
        JButton btnContinue = new JButton();
        btnContinue.setIcon(new ImageIcon("Asset/Stage/Continue.png"));
        btnContinue.addActionListener(e -> {
            Main.status = 6;
        });

        JButton btnExit = new JButton();
        btnExit.setIcon(new ImageIcon("Asset/Stage/Exit.png"));

        btnExit.addActionListener(e -> System.exit(1));
        setLayout(null);
        btnContinue.setBounds(350, 350, 495, 128);
        add(btnContinue);
        btnExit.setBounds(350, 500, 495, 128);
        add(btnExit);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Image imgBackground = new ImageIcon("Asset/Stage/NextLevel.png").getImage();
        g.drawImage(imgBackground, 0, 0, 1200, 800, null);
    }
}
