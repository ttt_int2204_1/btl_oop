package main;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class EndGame {
    public static EndGamePanel endGamePanel;
    public static void setEndGame(JFrame frame) {
        endGamePanel = new EndGamePanel();
        frame.add(endGamePanel);
    }
}

class EndGamePanel extends JPanel {
    public EndGamePanel() {
        JButton btnPlayAgain = new JButton();
        btnPlayAgain.setIcon(new ImageIcon("Asset/Stage/PlayAgain.png"));
        btnPlayAgain.addActionListener(e -> {
            Main.status = 2;
        });

        JButton btnExit = new JButton();
        btnExit.setIcon(new ImageIcon("Asset/Stage/Exit.png"));

        btnExit.addActionListener(e -> System.exit(1));
        setLayout(null);
        btnPlayAgain.setBounds(350, 350, 495, 128);
        add(btnPlayAgain);
        btnExit.setBounds(350, 500, 495, 128);
        add(btnExit);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Image imgBackground = new ImageIcon("Asset/Stage/EndGame.png").getImage();
        g.drawImage(imgBackground, 0, 0, 1200, 800, null);
    }
}
