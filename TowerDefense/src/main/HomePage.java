package main;

import javax.swing.*;
import java.awt.*;

public class HomePage {
    public static HomePagePanel homePagePanel;
    public static void setHomePage(JFrame frame) {
        homePagePanel = new HomePagePanel();
        frame.add(homePagePanel);
        frame.setVisible(true);
    }
}

class HomePagePanel extends JPanel {
    public HomePagePanel() {
        JButton btnPlayAgain = new JButton();
        btnPlayAgain.setIcon(new ImageIcon("Asset/Stage/Play.png"));
        btnPlayAgain.addActionListener(e -> {
            Main.status = 2;
        });

        JButton btnExit = new JButton();
        btnExit.setIcon(new ImageIcon("Asset/Stage/Exit.png"));

        btnExit.addActionListener(e -> System.exit(1));
        setLayout(null);
        btnPlayAgain.setBounds(350, 350, 495, 128);
        add(btnPlayAgain);
        btnExit.setBounds(350, 500, 495, 128);
        add(btnExit);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Image imgBackground = new ImageIcon("Asset/Stage/NewGame.png").getImage();
        g.drawImage(imgBackground, 0, 0, 1200, 800, null);
    }
}