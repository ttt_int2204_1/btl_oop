package main;

public class Player {
	private int energy;
	private int cash;
	private int level;

	public int getCash() {
		return cash;
	}

	public int getLevel() {
		return level;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}

	public void setPlayer(int energy, int cash, int level) {
		this.energy = energy;
		this.cash = cash;
		this.level = level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Player(int energy, int cash) {
		this.energy = energy;
		this.cash   = cash;
	}

	public Player() {}
	
	public int getEnergy() {
		return energy;
	}
	
	public void setEnergy(int energy) {
	    this.energy = energy;
	}

	public void reduceEnergy() {
		energy--;
	}
}
