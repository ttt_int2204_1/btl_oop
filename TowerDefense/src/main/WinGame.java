package main;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class WinGame {
    public static WinGamePanel winGamePanel;
    public static void setWinGame(JFrame frame) {
        winGamePanel = new WinGamePanel();
        frame.add(winGamePanel);
        frame.setVisible(true);
    }
}

class WinGamePanel extends JPanel {
    public WinGamePanel() {
        JButton btnContinue = new JButton();
        btnContinue.setIcon(new ImageIcon("Asset/Stage/Restart.png"));
        btnContinue.addActionListener(e -> {
            Main.status = 6;
        });

        JButton btnExit = new JButton();
        btnExit.setIcon(new ImageIcon("Asset/Stage/Exit.png"));

        btnExit.addActionListener(e -> System.exit(1));
        setLayout(null);
        btnContinue.setBounds(350, 350, 495, 128);
        add(btnContinue);
        btnExit.setBounds(350, 500, 495, 128);
        add(btnExit);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Image imgBackground = new ImageIcon("Asset/Stage/WinGame.png").getImage();
        g.drawImage(imgBackground, 0, 0, 1200, 800, null);
    }
}
