package gameentity;

import animation.Rotate;
import main.Player;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public abstract class Enemy extends JPanel {
	private static final long serialVersionUID = 1L;
	protected int x;
	protected int y;
	protected int speed;
	protected Image imgEnemy;
	protected int widthImg;
	protected int heightImg;
	protected int blood;
	protected int reward;
	protected int bloodMax;
	protected int key;
	protected int indexNextImportantKey;

	public Enemy(int x, int y, int speed, int widthImg, int heightImg, int bloodMax, int reward, int key) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.widthImg = widthImg;
		this.heightImg = heightImg;
		this.bloodMax = bloodMax;
		this.reward = reward;
		this.blood = this.bloodMax;
		this.indexNextImportantKey = 1;
		this.key = key;
	}

	public int getBlood() {
		return blood;
	}

	public void setBlood(int blood) {
		this.blood = blood;
	}

	public void reduceBlood(int num) {
		if (blood - num > 0)
			blood -= num;
		else blood = 0;
	}

	public int getReward() {
		return reward;
	}

	public void setReward(int reward) {
		this.reward = reward;
	}

	public int getBloodMax() {
		return bloodMax;
	}

	public void setBloodMax(int bloodMax) {
		this.bloodMax = bloodMax;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getWidthImg() {
		return heightImg;
	}

	public void setWidthImg(int widthImg) {
		this.widthImg = widthImg;
	}

	public int getHeightImg() {
		return heightImg;
	}

	public void setHeightImg(int heightImg) {
		this.heightImg = heightImg;
	}

	public Image getImgEnemy() {
		return imgEnemy;
	}

	public void setImgEnemy(Image imgEnemy) {
		this.imgEnemy = imgEnemy;
	}
	
	public void setImgEnemy(String pathImgEnemy) {
		ImageIcon temp = new ImageIcon(pathImgEnemy);
		imgEnemy = temp.getImage();
	}

	@Override
	public void paintComponent(Graphics g) {
		// draw Enemy
		//g.drawImage(imgEnemy, x, y, widthImg, heightImg, this);
		
		Rotate.changeAngle(g, imgEnemy, (90 * key) % 360, x, y, widthImg, heightImg);
		
		
		// draw blood bar
		g.drawImage((new ImageIcon("Asset/GameEntity/redBlood.png")).getImage(),   x, y - 8, widthImg, 							 	8, this);
		g.drawImage((new ImageIcon("Asset/GameEntity/greenBlood.png")).getImage(), x, y - 8, (int) (widthImg*blood/(1.0*bloodMax)), 8, this);
	}
	
	public void move(int[][] importantKey, Player player, List<Enemy> listEnemies) {
		// suppose: blood > 0
		if (indexNextImportantKey < importantKey.length) {
			if (key == 1) {
				// go up
				if (y - speed  > importantKey[indexNextImportantKey][1] * 64 + 64 + 10)
					y -= speed;
				else {
					y = importantKey[indexNextImportantKey++][1] * 64 + 64 + 10;
					if (indexNextImportantKey < importantKey.length) {
						if (x < importantKey[indexNextImportantKey][0] * 64 + 64 + 10) 
							key = 2;	// go right
						else key = 4; 	// go left
					} else {
						// lost 1 life
						listEnemies.remove(this);
						player.reduceEnergy();
					}
				}
			} else if (key == 2) {
				// go right
				if (x + speed < importantKey[indexNextImportantKey][0] * 64 + 64 + 10)
					x += speed;
				else {
					x = importantKey[indexNextImportantKey++][0] * 64 + 64 + 10;
					if (indexNextImportantKey < importantKey.length) {
						if (y < importantKey[indexNextImportantKey][1] * 64 + 64 + 10)
							key = 3;	// go down
						else key = 1; 	// go up
					} else {
						// lost 1 life
						listEnemies.remove(this);
						player.reduceEnergy();
					}
				}
			} else if (key == 3) {
				// go down
				if (y + speed  < importantKey[indexNextImportantKey][1] * 64 + 64 + 10)
					y += speed;
				else {
					y = importantKey[indexNextImportantKey++][1] * 64 + 64 + 10;
					if (indexNextImportantKey < importantKey.length) {
						if (x < importantKey[indexNextImportantKey][0] * 64 + 64 + 10)
							key = 2;	// go right
						else key = 4; 	// go left
					} else {
						// lost 1 life
						listEnemies.remove(this);
						player.reduceEnergy();
					}
				}
			} else if (key == 4) {
				// go left
				if (x - speed > importantKey[indexNextImportantKey][0] * 64 + 64 + 10)
					x -= speed;
				else {
					x = importantKey[indexNextImportantKey++][0] * 64 + 64 + 10;
					if (indexNextImportantKey < importantKey.length) {
						if (y < importantKey[indexNextImportantKey][1] * 64 + 64 + 10)
							key = 3;	// go down
						else key = 1; 	// go up
					} else {
						// lost 1 life
						listEnemies.remove(this);
						player.reduceEnergy();
					}
				}
			}
		} else {
			// lost 1 life
			listEnemies.remove(this);
			player.reduceEnergy();
		}
	}
	
}
