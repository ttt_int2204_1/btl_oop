package gameentity;

public class Gun2 extends Tower {
    private static final long serialVersionUID = 1L;

    private String[] listPathGun = {
            "Asset/GameEntity/Gun2_1.png",
            "Asset/GameEntity/Gun2_2.png",
            "Asset/GameEntity/Gun2_3.png",
            "Asset/GameEntity/Gun2_4.png"
    };

    private String[] listPathBullet = {
            "Asset/GameEntity/Bullet7.png",
            "Asset/GameEntity/Bullet7.png",
            "Asset/GameEntity/Bullet8.png",
            "Asset/GameEntity/Bullet8.png"
    };

    public Gun2(int x, int y) {
        this.x = x;
        this.y = y;

        keyLevel = 0;
        shootSpeed = 10;

        setImgPlatform();
        setImgGun(listPathGun[keyLevel]);
        setImgBullet(listPathBullet[keyLevel]);

        setUp();
    }

    private void setUp() {
        arrRadius = new int[] {100, 150, 200, 300};
        arrDamage = new int[] {20, 40, 80, 120};
        arrUpgrade = new int[] {50, 120, 200, 350};
        arrSell = new int[] {40, 100, 175, 300};
    }

    @Override
    public int getMaxLevel() {
        return 3;
    }

    @Override
    public void Upgrade() {
        keyLevel++;
        setImgPlatform();
        setImgGun(listPathGun[keyLevel]);
        setImgBullet(listPathBullet[keyLevel]);
    }
}
