package gameentity;

import animation.Rotate;

import javax.swing.*;
import java.awt.*;

public class Bullet extends JPanel {
    private int x;
    private int y;
    private Image image;
    private int speed;
    private int radius;
    private double degree; // in Deg
    private int x0;
    private int y0;
    private int X;
    private int Y;
    private double[] keyRandom = new double[] {0.5, 0.75, 1.0};

    public Bullet(int x0, int y0, Image image, int speed, int radius, double degree, Enemy enemy) {
        this.x = x0;
        this.x0 = x0;
        this.y = y0;
        this.y0 = y0;
        this.image = image;
        this.speed = speed;
        this.radius = radius;
        this.degree = degree;
        this.X = enemy.getX();
        this.Y = enemy.getY();
    }

    public boolean checkRadius() {
        return (x - x0)*(x - x0) + (y - y0)*(y - y0) <= ((X - x0)*(X - x0) + (Y - y0)*(Y - y0));
    }

    @Override
    public void paintComponent(Graphics g) {
        double k = (speed*speed*keyRandom[(int) (Math.random()*3)])/(Math.sqrt((X - x0)*(X - x0) + (Y - y0)*(Y - y0)));
        x = (int) (x + (X - x0)*k);
        y = (int) (y + (Y - y0)*k);
        if (checkRadius()) {
            Rotate.changeAngle(g, image, degree, x, y, 64, 64);
        }
    }
}
