package gameentity;

import animation.Rotate;
import main.GameStage;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

public abstract class Tower extends JPanel {
	private static final long serialVersionUID = 1L;
    protected int x;
	protected int y;
	protected int keyLevel; // from 0 to xxxx
	protected int shootSpeed;
	protected Image imgPlatform;
	protected Image imgGun;
	protected Image imgBullet;
	protected int[] arrRadius;
	protected int[] arrDamage;
	protected int[] arrUpgrade;
	protected int[] arrSell;
	protected List<Bullet> bulletList = new LinkedList<Bullet>();
	protected int status = 0;

	public abstract int getMaxLevel();
	public int getShootSpeed() {
		return shootSpeed;
	}
	public Image getImgBullet() {
		return imgBullet;
	}
	public void setImgBullet(String pathImgBullet) {
		ImageIcon temp = new ImageIcon(pathImgBullet);
		this.imgBullet = temp.getImage();
	}
	public int getRadius() {
		return arrRadius[keyLevel];
	}
	public int getDamage() {
		return arrDamage[keyLevel];
	}
	public int getUpgrade() {
		return arrUpgrade[keyLevel + 1];
	}
	public int getSell() {
		return arrSell[keyLevel];
	}
	public Image getImgPlatform() {
		return imgPlatform;
	}
	public void setImgPlatform() {
		ImageIcon temp = new ImageIcon("Asset/GameEntity/platformGun.png");
		this.imgPlatform = temp.getImage();
	}
	public Image getImgGun() {
		return imgGun;
	}
	public void setImgGun(String pathImgGun) {
		ImageIcon temp = new ImageIcon(pathImgGun);
		this.imgGun = temp.getImage();
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getKeyLevel() {
		return keyLevel;
	}
	public void setKeyLevel(int keyLevel) {
		this.keyLevel = keyLevel;
	}
	public abstract void Upgrade();
    
    public boolean isInRange(Enemy enemy) {
        double distance = Math.sqrt(Math.pow(enemy.getX() - x + enemy.getWidth()/2 - 64/2, 2) + 
                                    Math.pow(enemy.getY() - y + enemy.getHeight()/2 - 64/2, 2));
        return (distance <= arrRadius[keyLevel]);
    }

    public boolean canShoot(Enemy enemy) {
		if (bulletList.size() > 5)
			return false;
		if (status == 1 && bulletList.size() != 0) {
			return false;
		}
    	if (!isInRange(enemy))
			return false;
		if (enemy instanceof Plane && (this instanceof Gun1 || this instanceof Gun2))
			return false;
		if (!(enemy instanceof Plane) && this instanceof Gun3)
			return false;
		if (bulletList.size() == 3) status = 1; else status = 0;
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("Asset/sounds/shoot.wav").getAbsoluteFile( ));
			Clip clip = AudioSystem.getClip( );
			clip.open(audioInputStream);
			if (GameStage.sound)
				clip.start();
		}
		catch(Exception ex){
		}
		return true;
	}

    public double getAngleinRadian(Enemy enemy) {
        // vector a (0, -1);
        // vector b (x' - x + wid/2 - 64/2, y' - y + hei/2 - 64/2)
        // return angle(vector(a), vector(b))
        double scalar = y - enemy.getY() + 32 - enemy.getHeight()/2.0;
        double modult = Math.sqrt(Math.pow(x - enemy.getX() + 32 - enemy.getWidth()/2.0, 2) +  scalar*scalar);
		if (x < enemy.getX())
			return Math.acos(scalar/modult);
		else 
			return 2*Math.PI - Math.acos(scalar/modult);
	}

	public void printInfor(Graphics g) {
		g.setFont(new Font("TimesRoman", Font.PLAIN, 15));
		g.drawString("Level: " + (getKeyLevel() + 1), 990, 300);
		g.drawString("Damage: " + getDamage(), 990, 330);
		g.drawString("Sell Value: " + getSell(), 990, 360);
		if (keyLevel < 3) {
			g.drawString("Upgrade Damage: " + arrDamage[keyLevel + 1], 990, 390);
			g.drawString("Upgrade Cost: " + getUpgrade(), 990, 420);
		}

		g.drawRect(980, 280, 185, 150);
	}

	public static void printInfor(Graphics g, int key) {
    	g.drawImage(new ImageIcon("Asset/GameEntity/platformGun.png").getImage(), 1030, 260, 80, 80, null);
		g.drawImage(new ImageIcon("Asset/GameEntity/Gun" + key +"_1.png").getImage(), 1030, 260, 80, 80, null);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 15));
		String damage = "", attack = "", money = "";
		switch (key) {
			case 1:
				damage = "" + 10;
				attack = "Car, Tank";
				money = "$10";
				break;
			case 2:
				damage = "" + 20;
				attack = "Car, Tank";
				money = "$20";
				break;
			case 3:
				damage = "" + 30;
				attack = "Plane";
				money = "$50";
				break;
			case 4:
				damage = "" + 50;
				attack = "All";
				money = "$70";
				break;
		}
		g.drawString("Damage: " + damage, 990, 390);
		g.drawString("Attack: " + attack, 990, 420);
		g.drawString("Money: " + money, 990, 450);
		g.drawRect(980, 370, 185, 100);
	}

    public void paintComponent(Graphics g, double degreeGun, Enemy enemy) {
        g.drawImage(imgPlatform, x, y, this);
        Rotate.changeAngle(g, imgGun, degreeGun, x, y, 64, 64);
        if (enemy != null) {
			bulletList.add(new Bullet(x, y, imgBullet, shootSpeed, getRadius(), degreeGun, enemy));
		}
		for (int i = 0; i < bulletList.size(); i++) {
			Bullet bullet = bulletList.get(i);
			if (bullet.checkRadius())
				bullet.paintComponent(g);
			else bulletList.remove(bullet);
		}
    }
}
