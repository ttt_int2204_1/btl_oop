package gameentity;

public class Gun1 extends Tower {
    private static final long serialVersionUID = 1L;

    private String[] listPathGun = {
            "Asset/GameEntity/Gun1_1.png",
            "Asset/GameEntity/Gun1_1.png",
            "Asset/GameEntity/Gun1_2.png",
            "Asset/GameEntity/Gun1_2.png"
    };

    private String[] listPathBullet = {
            "Asset/GameEntity/Bullet1.png",
            "Asset/GameEntity/Bullet2.png",
            "Asset/GameEntity/Bullet3.png",
            "Asset/GameEntity/Bullet4.png"
    };

    public Gun1(int x, int y) {
        this.x = x;
        this.y = y;

        keyLevel = 0;
        shootSpeed = 7;

        setImgPlatform();
        setImgGun(listPathGun[keyLevel]);
        setImgBullet(listPathBullet[keyLevel]);

        setUp();
    }

    private void setUp() {
        arrRadius = new int[] {100, 150, 200, 300};
        arrDamage = new int[] {10, 15, 20, 30};
        arrUpgrade = new int[] {10, 20, 35, 60};
        arrSell = new int[] {8, 15, 25, 40};
    }

    @Override
    public int getMaxLevel() {
        return 3;
    }

    @Override
    public void Upgrade() {
        keyLevel++;
        setImgPlatform();
        setImgGun(listPathGun[keyLevel]);
        setImgBullet(listPathBullet[keyLevel]);
    }

}
