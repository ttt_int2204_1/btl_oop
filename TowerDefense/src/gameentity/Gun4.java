package gameentity;

public class Gun4 extends Tower {
    private static final long serialVersionUID = 1L;

    private String[] listPathGun = {
            "Asset/GameEntity/Gun4_1.png",
            "Asset/GameEntity/Gun4_1.png",
            "Asset/GameEntity/Gun4_2.png",
            "Asset/GameEntity/Gun4_2.png"
    };

    private String[] listPathBullet = {
            "Asset/GameEntity/Bullet9.png",
            "Asset/GameEntity/Bullet7.png",
            "Asset/GameEntity/Bullet10.png",
            "Asset/GameEntity/Bullet8.png"
    };

    public Gun4(int x, int y) {
        this.x = x;
        this.y = y;

        keyLevel = 0;
        shootSpeed = 14;

        setImgPlatform();
        setImgGun(listPathGun[keyLevel]);
        setImgBullet(listPathBullet[keyLevel]);

        setUp();
    }

    private void setUp() {
        arrRadius = new int[] {150, 200, 300, 350};
        arrDamage = new int[] {50, 80, 120, 200};
        arrUpgrade = new int[] {75, 100, 150, 250};
        arrSell = new int[] {50, 70, 120, 200};
    }

    @Override
    public int getMaxLevel() {
        return 3;
    }

    @Override
    public void Upgrade() {
        keyLevel++;
        setImgPlatform();
        setImgGun(listPathGun[keyLevel]);
        setImgBullet(listPathBullet[keyLevel]);
    }
}
