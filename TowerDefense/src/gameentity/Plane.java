package gameentity;

public class Plane extends Enemy {
	private static final long serialVersionUID = 1L;
	private String pathPlane = "Asset/GameEntity/plane.png";
	
	public Plane(int x, int y, int speed, int bloodMax, int reward, int key) {
		// width: 38px, height: 32px
		super(x, y, speed, 38, 32, bloodMax, reward, key);
		this.setImgEnemy(pathPlane);
	}
}
