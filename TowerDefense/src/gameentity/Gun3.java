package gameentity;

import java.awt.event.MouseEvent;

public class Gun3 extends Tower {
    private static final long serialVersionUID = 1L;

    private String[] listPathGun = {
            "Asset/GameEntity/Gun3_1.png",
            "Asset/GameEntity/Gun3_1.png",
            "Asset/GameEntity/Gun3_2.png",
            "Asset/GameEntity/Gun3_2.png"
    };

    private String[] listPathBullet = {
            "Asset/GameEntity/Bullet5.png",
            "Asset/GameEntity/Bullet5.png",
            "Asset/GameEntity/Bullet6.png",
            "Asset/GameEntity/Bullet6.png"
    };

    public Gun3(int x, int y) {
        this.x = x;
        this.y = y;

        keyLevel = 0;
        shootSpeed = 12;

        setImgPlatform();
        setImgGun(listPathGun[keyLevel]);
        setImgBullet(listPathBullet[keyLevel]);

        setUp();
    }

    private void setUp() {
        arrRadius = new int[] {100, 150, 200, 300};
        arrDamage = new int[] {30, 50, 140, 180};
        arrUpgrade = new int[] {50, 100, 150, 250};
        arrSell = new int[] {30, 70, 120, 200};
    }

    @Override
    public int getMaxLevel() {
        return 3;
    }

    @Override
    public void Upgrade() {
        keyLevel++;
        setImgPlatform();
        setImgGun(listPathGun[keyLevel]);
        setImgBullet(listPathBullet[keyLevel]);
    }
}
