package gameentity;

public class Tank extends Enemy {
	private static final long serialVersionUID = 1L;
	private String pathTank = "Asset/GameEntity/tank.png";
	
	public Tank(int x, int y, int speed, int bloodMax, int reward, int key) {
		// width: 35px, height: 20px
		super(x, y, speed, 35, 20, bloodMax, reward, key);
		this.setImgEnemy(pathTank);
	}
}
