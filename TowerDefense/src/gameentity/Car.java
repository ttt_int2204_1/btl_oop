package gameentity;

public class Car extends Enemy {
	private static final long serialVersionUID = 1L;

	private String[] listPathCar = {
		"Asset/GameEntity/car1.png",
		"Asset/GameEntity/car2.png",
		"Asset/GameEntity/car3.png",
		"Asset/GameEntity/car4.png",
		"Asset/GameEntity/car5.png",
		"Asset/GameEntity/car6.png",
		"Asset/GameEntity/car7.png",
		"Asset/GameEntity/car8.png",
		"Asset/GameEntity/car9.png"
	};
	
	private int indexPathImgCar;

	public Car(int x, int y, int speed, int bloodMax, int reward, int key) {
		// width: 44px, height: 31px
		super(x, y, speed, 44, 31, bloodMax, reward, key);
		indexPathImgCar = (int) (Math.random() * listPathCar.length);
		this.setImgEnemy(listPathCar[indexPathImgCar]);
	}
	
}
