package animation;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;

public class Rotate extends JPanel {
	private static final long serialVersionUID = 1L;

	public static void changeAngle(Graphics g, Image image, double degree, int x, int y, int width, int height) {
		Graphics2D g2d = (Graphics2D)g;
        AffineTransform origXform = g2d.getTransform();
        AffineTransform newXform = (AffineTransform)(origXform.clone());
        newXform.rotate(Math.toRadians(degree), x + width/2, y + height/2);
        g2d.setTransform(newXform);
        g2d.drawImage(image, x, y, null);
        g2d.setTransform(origXform);
	}
}
